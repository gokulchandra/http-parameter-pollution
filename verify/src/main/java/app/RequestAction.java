package app;

import java.math.BigInteger;


public class RequestAction {
    AllowedActions action;
    BigInteger amount;

    private final BigInteger MIN = BigInteger.valueOf(0);
    private final BigInteger MAX = BigInteger.valueOf(1000000);

    RequestAction(String action, String amount) {
        validateInput(action);
        validateInput(amount);
        BigInteger castAmount = new BigInteger(amount);
        isAmountInRange(castAmount);
        this.action = AllowedActions.valueOf(action.toLowerCase());
        this.amount = new BigInteger(amount);
    }

    void validateInput(String input) {
        if (input == null || input.isEmpty() || input.trim().length() == 0)
            throw new IllegalArgumentException("Invalid input: " + input);
    }

    void isAmountInRange(BigInteger amount) {
        if (amount.compareTo(MIN) < 0 || amount.compareTo(MAX) > 0)
            throw new IllegalArgumentException("Invalid amount: " + amount);
    }
}

