package app;

public enum AllowedActions {
    transfer("transfer"), withdraw("withdraw");

    final String action;

    AllowedActions(String action) {
        this.action = action;
    }

}
