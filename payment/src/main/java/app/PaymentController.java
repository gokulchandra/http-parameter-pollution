package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @RequestMapping("/")
    public String res(@RequestParam String action, @RequestParam String amount) {
        String msg;
        if (action.contains("withdraw")) {
            msg = "Fake Payment Controller: Successfully withdrew $" + amount;
        } else if (action.contains("transfer")) {
            msg = "Fake Payment Controller: Successfully transfered $" + amount;
        } else {
            msg = "Fake Payment Controller: Specify an action, withdraw or transfer";
        }
        return msg;
    }
}
